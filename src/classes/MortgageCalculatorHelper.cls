/**
* Helper method for the Mortgage Calculator
* @author 	Tanminder Rai
* @date 	Feb 13, 2017
*/
public with sharing class MortgageCalculatorHelper {

	public Double downPayment;
	public Double loanAmount;
	public Decimal paymentAmt;
	public String paymentSchedule;
	public Double amortizationPeriod;
	public Double interestRate;
	public Double interestRateOld;
	public final Integer MIN_AMORTIZATION_PERDIO = 5;
	public final Integer MAX_AMORTIZATION_PERIOD = 25;
	private final Double MAX_MORTGAGE_LOAN_ALLOWED = 1000000; //1 million
	private final Double DOWN_PAYMENT_PIVOT_AMT = 500000;
	private final Double DOWN_PAYMENT_PERCENT_LOW = 0.05;
	private final Double DOWN_PAYMENT_PERCENT_HIGH = 0.05;
	private Map<String, Integer> paymentScheduleMap = new Map<String, Integer>{'monthly'=> 1, 
																				'biweekly'=> 2, 
																				'weekly'=>4};

	/**
	 * Constructor
	 * Set defaults
	 */
	public MortgageCalculatorHelper(){

   		//Interest rate stored in custom settings (variables stored in cache memory for easy access)
       	Mortgage_Calculator_Defaults__c defaults = Mortgage_Calculator_Defaults__c.getInstance();
		interestRate = defaults.Interest_Rate__c;
	}

	/**
	 * validateDownPayment	Validate that the down payment entered is a valid amount
	 * 						Must be at least 5% of first $500k plus 10% of any amount above $500k (So $50k on a $750k mortgage)
	 * @return 				true if its valid, else false
	 */
	public Boolean validateDownPayment(){

    	Double minDownPayment;
    	if(loanAmount <= DOWN_PAYMENT_PIVOT_AMT){
    		minDownPayment = DOWN_PAYMENT_PIVOT_AMT * DOWN_PAYMENT_PERCENT_LOW;
    	}else {
    		minDownPayment = DOWN_PAYMENT_PIVOT_AMT * DOWN_PAYMENT_PERCENT_LOW;
    		minDownPayment += (loanAmount - DOWN_PAYMENT_PIVOT_AMT)*DOWN_PAYMENT_PERCENT_HIGH;
    	}
    	return downPayment > minDownPayment;
	}

	/**
	 * validateAmortizationPeriod	Validate that the amortization period entered is a valid amount
	 *								min 5 years, max 25 years
	 * @return 						true if its valid, else false
	 */
	public Boolean validateAmortizationPeriod(){
    	return (amortizationPeriod > MIN_AMORTIZATION_PERDIO) && (amortizationPeriod < MAX_AMORTIZATION_PERIOD) ;
	}

	/**
	 * calculatePaymentAmount	Calculate the total mortgage payments by payment schedule (monthly, weekly, biweely)
	 *							includes insurance costs
	 * @return 					payment amount 
	 */
	public Double calculatePaymentAmount(){
		
		//calculate insurance
		Double insurancePercent = calculateMortgageInsurance();
		loanAmount += loanAmount * insurancePercent;

		//calculate the payment amount per month
		paymentAmt = loanAmount * calcMortgage();

		//divide by payment schedule if weekly or biweekly
		Double period = paymentScheduleMap.get( paymentSchedule.toLowerCase() );		
		paymentAmt = paymentAmt/period;	

		return paymentAmt.setScale(2);
	}

	/**
	 * calculateMaxMortgageAmount	Calculate the max mortgage amount
	 * @return 						Max mortgage amount
	 */
	public Double calculateMaxMortgageAmount(){

		Decimal maxMortgageAmount = paymentAmt/calcMortgage();

		//if down payment is given, add to max mortgage amount 
		if (downPayment!=null) maxMortgageAmount += downPayment;

		return maxMortgageAmount.setScale(2);
	}

	/**
	 * updateInterestRate	Updates the interest rate
	 */
	public void updateInterestRate(){

     	Mortgage_Calculator_Defaults__c defaults = Mortgage_Calculator_Defaults__c.getOrgDefaults();

		//set old interest rate
		interestRateOld = defaults.Interest_Rate__c;

    	//update the interest rate in the database
		defaults.Interest_Rate__c = interestRate;
		upsert defaults Mortgage_Calculator_Defaults__c.Id;  
	}

	/**
	 * calcMortgage	 Mortgage calculation
	 */
    private Double calcMortgage(){
    	Double c = interestRate / 1200;
    	Double n = amortizationPeriod * 12;
    	return (c* Math.pow( (1+c), n)) /(Math.pow((1+c),n)-1);
    }

	/**
	 * calculateMortgageInsurance	Calculate mortgage insurance
	 * @return 						Mortgage insurance
	 */
	private Double calculateMortgageInsurance(){

		//Insurance not available for mortgages over certain amount eg. 1 million
		if(loanAmount > MAX_MORTGAGE_LOAN_ALLOWED) return null;

		Double downPaymentPercent = downPayment/loanAmount * 100; 
		Double insurancePercent;
		if(downPaymentPercent >= 5 && downPaymentPercent <= 9.99){
			insurancePercent = 3.15;
		}else if(downPaymentPercent >= 10 && downPaymentPercent <= 14.99){
			insurancePercent = 2.4;
		}else if(downPaymentPercent >= 15 && downPaymentPercent <= 19.99){
			insurancePercent = 1.8;
		}
		return insurancePercent/100;
	}
}