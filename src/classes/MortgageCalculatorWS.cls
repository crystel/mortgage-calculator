/**
* Web service to calculate the mortgage payments, max mortgage amount and update interest rate
* assumption: amortization period given in years
* assumption: asking price is the loan amount, not the loan amount + down payment
* @author 	Tanminder Rai
* @date 	Feb 13, 2017
*/
@RestResource(urlMapping='/mortgage-calculator/*')
global class MortgageCalculatorWS {

    /**
     * getAmount 	Get the payment or max mortgage amount 	
     * @return		JSON string with the payment amount or max mortgage amount
     */    
    @HttpGet
    global static String getAmount() {

    	MortgageCalculatorHelper helper = new MortgageCalculatorHelper();

		//Get params from REST request
    	RestRequest request = RestContext.request;
    	if(request.params.get('askingPrice') != null ){
    		helper.loanAmount = Double.valueOf(request.params.get('askingPrice'));
    	}else{
            return 'Please enter the asking price';
        }
    	if(request.params.get('paymentAmount') != null){
    		helper.paymentAmt = Double.valueOf(request.params.get('paymentAmount'));
    	}
    	if(request.params.get('downPayment') != null){
    		helper.downPayment = Double.valueOf(request.params.get('downPayment'));
    	}
    	if(request.params.get('amortizationPeriod') != null){
    		helper.amortizationPeriod = Double.valueOf(request.params.get('amortizationPeriod') );
    	}else{
            return 'Please enter the amortization period';
        }
        if(request.params.get('paymentSchedule') != null){
    	   helper.paymentSchedule 	= request.params.get('paymentSchedule');
        }else{
            return 'Please enter a payment schedule';
        }

    	//if down payment is not valid, return error message
    	if(helper.validateDownPayment()){
    		return 'Down paymen must be at least 5% of first $500k plus 10% of any amount above $500k';
    	}

    	//if amortization period is not valid, return error message
    	if(!helper.validateAmortizationPeriod()){
    		return 'Amortization Period not valid. Should be between ' + helper.MIN_AMORTIZATION_PERDIO + 
    				' to ' + helper.MAX_AMORTIZATION_PERIOD + ' years.';
    	}

    	//get url substring
		String url = request.requestURI.substring(         
						 request.requestURI.lastIndexOf('/')+1);

		//if the request is for payment amount, calculate payments
		if(url == 'payment-amount'){
    		return String.valueOf( helper.calculatePaymentAmount() );

    	//if the request is for mortgage amount, calculate max mortgage
		}else if(url == 'mortgage-amount'){
    		return String.valueOf( helper.calculateMaxMortgageAmount() );
		}
    	return null;
    }

    /**
     * updateInterestRate 	Updates the interest rate in the database
     * @return  			Message with the old and updated interest rate
     */
    @HttpPatch(urlMapping='/interest-rate/*')
    global static String updateInterestRate() {

    	RestRequest request = RestContext.request;	
	    MortgageCalculatorHelper helper = new MortgageCalculatorHelper();

	    //set interest rate
	    helper.interestRate = Double.valueOf(request.params.get('interestRate'));

	    //update interest rate
	    helper.updateInterestRate();

		//return message
    	return 'Interest rate has been updated from ' + String.valueOf(helper.interestRateOld) 
    				+ ' to ' + String.valueOf(helper.interestRate);
    }	


}