@isTest
private class MortgageCalculatorHelper_Test{
	//test that 
	@isTest
	static void validateDownPaymentTest(){

		MortgageCalculatorHelper calc = new MortgageCalculatorHelper();
		calc.loanAmount = 500000;
		calc.downPayment = 5;
		calc.amortizationPeriod = 20;
		calc.paymentSchedule = 'weekly';

		Test.startTest();
		Boolean result = calc.validateDownPayment();
		Test.stopTest();

		System.assertEquals(result, false);
	}

	@isTest
	static void validateAmortizationPeriodTest(){

		MortgageCalculatorHelper calc = new MortgageCalculatorHelper();
		calc.loanAmount = 500000;
		calc.downPayment = 25000;
		calc.amortizationPeriod = 1;
		calc.paymentSchedule = 'weekly';

		Test.startTest();
		Boolean result = calc.validateAmortizationPeriod();
		Test.stopTest();

		System.assertEquals(result, false);
	}

	//test that 
	@isTest
	static void calculatePaymentTest(){

		MortgageCalculatorHelper calc = new MortgageCalculatorHelper();
		calc.loanAmount = 500000;
		calc.downPayment = 25000;
		calc.amortizationPeriod = 20;
		calc.paymentSchedule = 'weekly';

		Test.startTest();
		calc.calculatePaymentAmount();
		Test.stopTest();

		System.assertEquals(calc.paymentAmt, 683.24);
	}

	@isTest
	static void calculateMaxMortgageTest(){

		MortgageCalculatorHelper calc = new MortgageCalculatorHelper();
		calc.loanAmount = 500000;
		calc.downPayment = 25000;
		calc.amortizationPeriod = 20;
		calc.paymentSchedule = 'weekly';

		Test.startTest();
		Double result = calc.calculateMaxMortgageAmount();
		Test.stopTest();

		System.assertEquals(result, 308070.73);
	}



}